# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Form(models.Model):
    _name = 'odoo_qweb.form'
    _description = 'Formulario'

    name = fields.Char(string='Nombre')
    surnames = fields.Char(string='Apellidos')
    address = fields.Char(string='Dirección')
    phone = fields.Char(string='Teléfono')
    email = fields.Char(string='Email')


class FormReport(models.AbstractModel):
    _name = 'report.odoo_qweb.report_form'

    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('odoo_qweb.report_form')
        return {
            'doc_ids': docids,
            'doc_model': self.env['odoo_qweb.form'],
            'docs': self.env['odoo_qweb.form'].browse(docids)
        }