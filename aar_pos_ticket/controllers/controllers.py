# -*- coding: utf-8 -*-
import json
from odoo import http
from odoo.http import Response
from odoo.http import request

#from relacion_timbrado_order.models.models import RelacionTimbradoFact


class FacturaController(http.Controller):
    @http.route("/check_method_get", auth='none', method=['GET'])
    def check_method_get(self, **values):
        output = {
            'results': {
                'code': 200,
                'message': 'OK'
            }
        }
        return 'hola'

    @http.route('/certificados', auth='public')
    def index(self, **kw):
        print('index')
        # return 'hola'
        try:
            return Response(json.dumps('hola', ensure_ascii=False).encode('utf-8'),
                            content_type='application/json;charset=utf-8', status=200)
        except Exception as e:
            return Response(json.dumps({'error': str(e)}),
                            content_type='application/json;charset=utf-8', status=505)


    @http.route('/prueba/<a>/<b>', auth='public', website=True)
    def prueba(self,a,b, **kw):
        # a = self[0].company_id.id

        aaa = http.request.env['res.company'].search([('id', '=', 1)])

        # datos = request.params['nombre']
        # datos2 = request.params['apellido']

        print('&&&&&&')
        # print(datos)
        # print(datos2)
        print(aaa[0].ultimo_numero_completo)


        print(a)
        print(b)

        relacionnn = {
            'venta': a,
            'factura': aaa[0].ultimo_numero_completo
        }
        print(relacionnn)
        aaa = http.request.env['relacion_timbrado_order.relaciontimbfact'].sudo().create(relacionnn)

        # datos = request.params['nombre']
        # datos2 = request.params['apellido']

        print('&&&&&&')
        # print(datos)
        # print(datos2)

        return "hola desde el controller"

    @http.route('/prueba4', auth='public', website=True)
    def prueba4(self, **kw):
        # a = self[0].company_id.id

        aaa = http.request.env['res.company'].search([('id', '=', 1)])

        # datos = request.params['nombre']
        # datos2 = request.params['apellido']

        print('&&&&&&')
        # print(datos)
        # print(datos2)
        print(aaa[0].ultimo_numero_completo)

        return aaa[0].ultimo_numero_completo

    @http.route('/prueba5', auth='public', website=True)
    def prueba5(self, **kw):
        # a = self[0].company_id.id

        aaa = http.request.env['res.company'].search([('id', '=', 1)])

        # datos = request.params['nombre']
        # datos2 = request.params['apellido']

        print('&&&&&&')
        # print(datos)
        # print(datos2)
        print(aaa[0].ultimo_numero_completo)

        return aaa[0].ultimo_numero_completo

    @http.route('/prueba2023/<a>', auth='public', website=True)
    def prueba5555(self,a, **kw):

        aaa = http.request.env['pos.order'].search([('pos_reference', '=', a)])
        print(aaa)
        return aaa.order_fact

    @http.route('/prueba20232/<a>', auth='public', website=True)
    def pruebaprueba20232(self, a, **kw):

        enlatabla = http.request.env['pos.order'].search([('pos_reference', '=', a)])
        print(enlatabla)

        calculado = http.request.env['res.company'].search([('id', '=', 1)])
        print('&&&&&&')
        print(calculado[0].ultimo_numero_completo)

        data = {
            'enlatabla':enlatabla.order_fact,
            'calculado':calculado.ultimo_numero_completo
        }

        return Response(json.dumps(data),
                        content_type='application/json;charset=utf-8')

    # @http.route('/api/factura', auth='public', method=['GET'], csrf=False)
    # def get_factura(self, **kw):
    #     try:
    #         visits = http.request.env['custom_crm.visit'].sudo().search_read([], ['id', 'name', 'customer', 'done'])
    #         res = json.dumps(visits, ensure_ascii=False).encode('utf-8')
    #         return Response(res, content_type='application/json;charset=utf-8', status=200)
    #     except Exception as e:
    #         return Response(json.dumps({'error': str(e)}), content_type='application/json;charset=utf-8', status=505)
    #
    # @http.route('/api/timbrado', auth='public', method=['GET'], csrf=False)
    # def calcularTimbradoAjax(self):
    #     a = self[0].company_id.id
    #     company1 = self.env['res.company'].browse([a])
    #     company = self.env['res.company'].browse([a])
    #     print(company)
    #     variable_actual = company.ultimo_numero_impreso
    #     company.ultimo_numero_impreso = company.ultimo_numero_impreso + 1
    #
    #     ult_imp = str(company.ultimo_numero_impreso)
    #     suc = str(company.numero_sucursal)
    #     caj = str(company.numero_caja)
    #
    #     # ult_imp = '00000000' + ult_imp
    #     ult_imp = '00000000' + str(variable_actual)
    #     suc = '000'+str(suc)
    #     caj = '000'+str(caj)
    #
    #     ult_imp = ult_imp[-8:]
    #     suc = suc[-3:]
    #     caj = caj[-3:]
    #
    #     print(ult_imp)
    #     print(suc)
    #     print(caj)
    #     company.ultimo_numero_completo = suc + '-' + caj + '-' + ult_imp
    #     print(company.ultimo_numero_completo)
    #     company.write({'ultimo_numero_impreso': company.ultimo_numero_impreso, 'ultimo_numero_completo': company.ultimo_numero_completo})
    #
    #     return Response(json.dumps({'ultimo_numero_completo': company.ultimo_numero_completo}), content_type='application/json;charset=utf-8')


    @http.route('/puntomiles/<a>', auth='public', website=True)
    def puntomiles(self,a, **kw):
        print(a)
        a = int(a)
        b = round(a, 0)
        print(b)
        print("{:,}".format(b))
        print("{:,}".format(b).replace(',', '~').replace('.', ',').replace('~', '.'))
        return "{:,}".format(b).replace(',', '~').replace('.', ',').replace('~', '.')
